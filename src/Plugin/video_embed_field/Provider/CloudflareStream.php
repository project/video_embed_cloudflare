<?php

namespace Drupal\video_embed_cloudflare\Plugin\video_embed_field\Provider;

use Drupal\video_embed_field\ProviderPluginBase;

/**
 * A Cloudflare Stream provider plugin for video embed field.
 *
 * @VideoEmbedProvider(
 *   id = "cloudflare_stream",
 *   title = @Translation("Cloudflare Stream")
 * )
 */
class CloudflareStream extends ProviderPluginBase {

  /**
   * {@inheritdoc}
   */
  public static function getIdFromInput($input) {
    preg_match('/^https?:\/\/(watch|iframe).videodelivery.net\/(?<id>[0-9a-zA-Z-_]{2,32}).*?(\?startTime=(\d+))?$/', $input, $matches);
    return $matches['id'] ?? FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function renderEmbedCode($width, $height, $autoplay) {
    $iframe = [
      '#type' => 'video_embed_iframe',
      '#provider' => 'cloudflare_stream',
      '#url' => sprintf('https://iframe.videodelivery.net/%s', $this->getVideoId()),
      '#query' => [],
      '#attributes' => [
        'width' => $width,
        'height' => $height,
        'frameborder' => '0',
        'allowfullscreen' => 'true',
        'allow' => 'accelerometer; gyroscope; picture-in-picture;',
        'preload' => 'preload',
      ],
    ];
    if ($time_index = $this->getTimeIndex()) {
      $iframe['#query']['startTime'] = sprintf('%s', $time_index);
    }
    return $iframe;
  }

  /**
   * Get the time index for when the given video starts.
   *
   * @return int
   *   The time index where the video should start based on the URL.
   */
  protected function getTimeIndex() {
    preg_match('/[&\?]startTime=((?<hours>\d+)h)?((?<minutes>\d+)m)?(?<seconds>\d+)s?/', $this->getInput(), $matches);
    $hours = !empty($matches['hours']) ? $matches['hours'] : 0;
    $minutes = !empty($matches['minutes']) ? $matches['minutes'] : 0;
    $seconds = !empty($matches['seconds']) ? $matches['seconds'] : 0;
    return $hours * 3600 + $minutes * 60 + $seconds;
  }

  /**
   * {@inheritdoc}
   */
  public function getRemoteThumbnailUrl() {
    // @todo Implement generating animated thumbnails.
    // https://videodelivery.net/%s/thumbnails/thumbnail.gif?time=3s&height=200&duration=4s
    $url = 'https://videodelivery.net/%s/thumbnails/thumbnail.jpg';
    return sprintf($url, $this->getVideoId());
  }

}
