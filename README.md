CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Usage
 * Troubleshooting
 * Project members


INTRODUCTION
------------

Cloudflare Stream is an easy-to-use, affordable, on-demand video streaming
platform. Stream seamlessly integrates video storage, encoding, and a
customizable player with Cloudflare’s fast, secure, and reliable global network,
so that you can spend less time managing video delivery and more time building
and promoting your product.

This modules integrates Cloudflare Stream as a supported source for the contrib
module Video Embed Field. Which will support the Cloudflare Stream as a host for
the video URLs that you will use with the Video Embed Field.


REQUIREMENTS
------------

Installed Video Embed Field. And a field created with the type of
video_embed_field.


INSTALLATION
------------

Install as you would normally install a contributed Drupal module. Visit
https://www.drupal.org/node/1897420 for further information.
You will need to enable the Cloudflare Stream from your custom field settings.


CONFIGURATION
-------------

You can configure the displyed field as you configure the video_embed_field
from the field display settings where you will be able to auto play the video.


USAGE
-------------

Add the Video Embed Field to any node or entity, enable the Cloudflare Stream
for this field. Once you insert a video from the Cloudflare Stream the video
will be streamed via their service.


TROUBLESHOOTING
-------------

* If there is an unexpected issue, please check the issue queue:
  https://www.drupal.org/project/issues/video_embed_cloudflare

* If there is an undocumented issue, please create one:
  https://www.drupal.org/node/add/project-issue/video_embed_cloudflare


PROJECT MEMBERS
-----------

See the list of current project members on
https://git.drupalcode.org/project/video_embed_cloudflare/-/project_members
